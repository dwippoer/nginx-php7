#!/bin/bash
nginx_dir1="/etc/nginx"
nginx_dir2="/etc/nginx/conf.d"
vhost_dir="/home/vhost1"
temp_dir="/tmp/install"
#update centos
sudo yum -y update

#install tools
sudo yum -y install git zip unzip wget vim nano

#update timezone
sudo rm /etc/localtime
sudo ln -s /usr/share/zoneinfo/Asia/Jakarta /etc/localtime

#instal epel & remi
sudo rpm -ivh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
sudo rpm -ivh http://rpms.famillecollet.com/enterprise/remi-release-7.rpm

#update centos
sudo yum -y update

#install nginx
sudo yum -y install nginx
sudo systemctl start nginx
sudo systemctl enable nginx

#sync repo
if [ -d $temp_dir ];
then
	rm -rf $temp_dir;
else 
	git clone https://dwippoer@bitbucket.org/dwippoer/nginx-php7.git;
fi

